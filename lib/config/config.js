"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Config = /** @class */ (function () {
    function Config() {
        this.mongodbURI = "mongodb://localhost:27017/vcaption";
        this.port = 4300;
    }
    return Config;
}());
exports.Config = Config;
